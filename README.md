# Field Formatter File Size

Field Formatter File Size is a field formatter to display the file size.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/field_formatter_file_size).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/field_formatter_file_size).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- AlGab - [algab](https://www.drupal.org/u/algab)
- Guillaume Jiguel - [Guietc](https://www.drupal.org/u/guietc)
- Guillaume Cambon - [becassin](https://www.drupal.org/u/becassin)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
